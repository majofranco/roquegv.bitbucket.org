json = [{"cod_uni_acad":1,"uni_acad":"FacultaddeArquitectura,DiseñoyArtes","anio":2010,"egre":44,"ingre":360,"postu":715,"ejec":18708455866},{"cod_uni_acad":2,"uni_acad":"FacultaddeCienciasAgrarias","anio":2010,"egre":351,"ingre":607,"postu":1389,"ejec":39599908138},{"cod_uni_acad":3,"uni_acad":"FacultaddeCienciasEconómicas","anio":2010,"egre":846,"ingre":1418,"postu":2832,"ejec":41367904226},{"cod_uni_acad":4,"uni_acad":"FacultaddeCienciasExactasyNaturales","anio":2010,"egre":148,"ingre":541,"postu":534,"ejec":19467950803},{"cod_uni_acad":5,"uni_acad":"FacultaddeCienciasMédicas","anio":2010,"egre":176,"ingre":232,"postu":964,"ejec":214116506290},{"cod_uni_acad":6,"uni_acad":"FacultaddeCienciasQuímicas","anio":2010,"egre":142,"ingre":182,"postu":470,"ejec":16269253412},{"cod_uni_acad":7,"uni_acad":"FacultaddeCienciasVeterinarias","anio":2010,"egre":177,"ingre":483,"postu":608,"ejec":35508603688},{"cod_uni_acad":8,"uni_acad":"FacultaddeDerechoyCienciasSociales","anio":2010,"egre":1558,"ingre":1345,"postu":1769,"ejec":27277023855},{"cod_uni_acad":9,"uni_acad":"FacultaddeFilosofia","anio":2010,"egre":357,"ingre":767,"postu":1066,"ejec":22941610394},{"cod_uni_acad":10,"uni_acad":"FacultaddeIngeniería","anio":2010,"egre":90,"ingre":253,"postu":406,"ejec":24323862848},{"cod_uni_acad":11,"uni_acad":"FacultaddeOdontología","anio":2010,"egre":40,"ingre":61,"postu":267,"ejec":10265565684},{"cod_uni_acad":12,"uni_acad":"FacultadPolitécnica","anio":2010,"egre":169,"ingre":544,"postu":1298,"ejec":35940187301},{"cod_uni_acad":13,"uni_acad":"Instituto\"Dr.AndrésBarbero\"","anio":2010,"egre":179,"ingre":356,"postu":605,"ejec":15103815278},{"cod_uni_acad":14,"uni_acad":"InstitutodeTrabajoSocial","anio":2010,"egre":0,"ingre":0,"postu":0,"ejec":0},{"cod_uni_acad":1,"uni_acad":"FacultaddeArquitectura,DiseñoyArtes","anio":2011,"egre":126,"ingre":404,"postu":744,"ejec":23484184250},{"cod_uni_acad":2,"uni_acad":"FacultaddeCienciasAgrarias","anio":2011,"egre":366,"ingre":616,"postu":1658,"ejec":46750186796},{"cod_uni_acad":3,"uni_acad":"FacultaddeCienciasEconómicas","anio":2011,"egre":960,"ingre":1583,"postu":2925,"ejec":48459534446},{"cod_uni_acad":4,"uni_acad":"FacultaddeCienciasExactasyNaturales","anio":2011,"egre":161,"ingre":542,"postu":556,"ejec":27389892307},{"cod_uni_acad":5,"uni_acad":"FacultaddeCienciasMédicas","anio":2011,"egre":194,"ingre":224,"postu":1015,"ejec":228721244426},{"cod_uni_acad":6,"uni_acad":"FacultaddeCienciasQuímicas","anio":2011,"egre":188,"ingre":277,"postu":803,"ejec":17707599700},{"cod_uni_acad":7,"uni_acad":"FacultaddeCienciasVeterinarias","anio":2011,"egre":234,"ingre":396,"postu":696,"ejec":43926029115},{"cod_uni_acad":8,"uni_acad":"FacultaddeDerechoyCienciasSociales","anio":2011,"egre":1543,"ingre":1360,"postu":2109,"ejec":32875587816},{"cod_uni_acad":9,"uni_acad":"FacultaddeFilosofia","anio":2011,"egre":366,"ingre":792,"postu":1029,"ejec":27008836032},{"cod_uni_acad":10,"uni_acad":"FacultaddeIngeniería","anio":2011,"egre":160,"ingre":170,"postu":448,"ejec":31383848268},{"cod_uni_acad":11,"uni_acad":"FacultaddeOdontología","anio":2011,"egre":51,"ingre":61,"postu":268,"ejec":12975329763},{"cod_uni_acad":12,"uni_acad":"FacultadPolitécnica","anio":2011,"egre":184,"ingre":570,"postu":1297,"ejec":44338939195},{"cod_uni_acad":13,"uni_acad":"Instituto\"Dr.AndrésBarbero\"","anio":2011,"egre":176,"ingre":294,"postu":639,"ejec":20810593388},{"cod_uni_acad":14,"uni_acad":"InstitutodeTrabajoSocial","anio":2011,"egre":0,"ingre":0,"postu":0,"ejec":0},{"cod_uni_acad":1,"uni_acad":"FacultaddeArquitectura,DiseñoyArtes","anio":2012,"egre":77,"ingre":461,"postu":958,"ejec":31403986082},{"cod_uni_acad":2,"uni_acad":"FacultaddeCienciasAgrarias","anio":2012,"egre":430,"ingre":635,"postu":1676,"ejec":52103157762},{"cod_uni_acad":3,"uni_acad":"FacultaddeCienciasEconómicas","anio":2012,"egre":899,"ingre":1526,"postu":3086,"ejec":55712000391},{"cod_uni_acad":4,"uni_acad":"FacultaddeCienciasExactasyNaturales","anio":2012,"egre":177,"ingre":555,"postu":588,"ejec":31848597208},{"cod_uni_acad":5,"uni_acad":"FacultaddeCienciasMédicas","anio":2012,"egre":175,"ingre":240,"postu":933,"ejec":342765755523},{"cod_uni_acad":6,"uni_acad":"FacultaddeCienciasQuímicas","anio":2012,"egre":234,"ingre":230,"postu":488,"ejec":18498121838},{"cod_uni_acad":7,"uni_acad":"FacultaddeCienciasVeterinarias","anio":2012,"egre":207,"ingre":443,"postu":725,"ejec":56524582309},{"cod_uni_acad":8,"uni_acad":"FacultaddeDerechoyCienciasSociales","anio":2012,"egre":802,"ingre":1105,"postu":1596,"ejec":42979555625},{"cod_uni_acad":9,"uni_acad":"FacultaddeFilosofia","anio":2012,"egre":299,"ingre":779,"postu":985,"ejec":31521764945},{"cod_uni_acad":10,"uni_acad":"FacultaddeIngeniería","anio":2012,"egre":123,"ingre":291,"postu":328,"ejec":42609516804},{"cod_uni_acad":11,"uni_acad":"FacultaddeOdontología","anio":2012,"egre":55,"ingre":114,"postu":407,"ejec":13798684017},{"cod_uni_acad":12,"uni_acad":"FacultadPolitécnica","anio":2012,"egre":235,"ingre":508,"postu":1311,"ejec":54786143898},{"cod_uni_acad":13,"uni_acad":"Instituto\"Dr.AndrésBarbero\"","anio":2012,"egre":250,"ingre":317,"postu":615,"ejec":26338572870},{"cod_uni_acad":14,"uni_acad":"InstitutodeTrabajoSocial","anio":2012,"egre":22,"ingre":58,"postu":67,"ejec":0},{"cod_uni_acad":1,"uni_acad":"FacultaddeArquitectura,DiseñoyArtes","anio":2013,"egre":83,"ingre":499,"postu":1242,"ejec":31442258647},{"cod_uni_acad":2,"uni_acad":"FacultaddeCienciasAgrarias","anio":2013,"egre":400,"ingre":461,"postu":1175,"ejec":53944445312},{"cod_uni_acad":3,"uni_acad":"FacultaddeCienciasEconómicas","anio":2013,"egre":974,"ingre":1442,"postu":2881,"ejec":60455046453},{"cod_uni_acad":4,"uni_acad":"FacultaddeCienciasExactasyNaturales","anio":2013,"egre":162,"ingre":590,"postu":630,"ejec":34074838880},{"cod_uni_acad":5,"uni_acad":"FacultaddeCienciasMédicas","anio":2013,"egre":209,"ingre":240,"postu":992,"ejec":383237297044},{"cod_uni_acad":6,"uni_acad":"FacultaddeCienciasQuímicas","anio":2013,"egre":212,"ingre":231,"postu":663,"ejec":22499432277},{"cod_uni_acad":7,"uni_acad":"FacultaddeCienciasVeterinarias","anio":2013,"egre":295,"ingre":599,"postu":819,"ejec":58392378763},{"cod_uni_acad":8,"uni_acad":"FacultaddeDerechoyCienciasSociales","anio":2013,"egre":1015,"ingre":1257,"postu":1653,"ejec":61344638254},{"cod_uni_acad":9,"uni_acad":"FacultaddeFilosofia","anio":2013,"egre":231,"ingre":682,"postu":1062,"ejec":34133805029},{"cod_uni_acad":10,"uni_acad":"FacultaddeIngeniería","anio":2013,"egre":147,"ingre":295,"postu":371,"ejec":44890365081},{"cod_uni_acad":11,"uni_acad":"FacultaddeOdontología","anio":2013,"egre":52,"ingre":62,"postu":364,"ejec":15383065396},{"cod_uni_acad":12,"uni_acad":"FacultadPolitécnica","anio":2013,"egre":206,"ingre":484,"postu":1391,"ejec":63033018912},{"cod_uni_acad":13,"uni_acad":"Instituto\"Dr.AndrésBarbero\"","anio":2013,"egre":253,"ingre":377,"postu":628,"ejec":27682462579},{"cod_uni_acad":14,"uni_acad":"InstitutodeTrabajoSocial","anio":2013,"egre":51,"ingre":50,"postu":53,"ejec":0},{"cod_uni_acad":1,"uni_acad":"FacultaddeArquitectura,DiseñoyArtes","anio":2014,"egre":46,"ingre":460,"postu":1333,"ejec":36772249136},{"cod_uni_acad":2,"uni_acad":"FacultaddeCienciasAgrarias","anio":2014,"egre":367,"ingre":636,"postu":1657,"ejec":53589283711},{"cod_uni_acad":3,"uni_acad":"FacultaddeCienciasEconómicas","anio":2014,"egre":939,"ingre":1610,"postu":2736,"ejec":0},{"cod_uni_acad":4,"uni_acad":"FacultaddeCienciasExactasyNaturales","anio":2014,"egre":154,"ingre":515,"postu":540,"ejec":38675457456},{"cod_uni_acad":5,"uni_acad":"FacultaddeCienciasMédicas","anio":2014,"egre":111,"ingre":286,"postu":1100,"ejec":419492614347},{"cod_uni_acad":6,"uni_acad":"FacultaddeCienciasQuímicas","anio":2014,"egre":132,"ingre":213,"postu":499,"ejec":20343277005},{"cod_uni_acad":7,"uni_acad":"FacultaddeCienciasVeterinarias","anio":2014,"egre":270,"ingre":551,"postu":785,"ejec":66712069262},{"cod_uni_acad":8,"uni_acad":"FacultaddeDerechoyCienciasSociales","anio":2014,"egre":865,"ingre":1425,"postu":1721,"ejec":62013455339},{"cod_uni_acad":9,"uni_acad":"FacultaddeFilosofia","anio":2014,"egre":455,"ingre":776,"postu":1427,"ejec":34791701221},{"cod_uni_acad":10,"uni_acad":"FacultaddeIngeniería","anio":2014,"egre":163,"ingre":412,"postu":501,"ejec":57121641391},{"cod_uni_acad":11,"uni_acad":"FacultaddeOdontología","anio":2014,"egre":52,"ingre":62,"postu":361,"ejec":15111930031},{"cod_uni_acad":12,"uni_acad":"FacultadPolitécnica","anio":2014,"egre":179,"ingre":716,"postu":1201,"ejec":67131909740},{"cod_uni_acad":13,"uni_acad":"Instituto\"Dr.AndrésBarbero\"","anio":2014,"egre":204,"ingre":411,"postu":568,"ejec":32744910102},{"cod_uni_acad":14,"uni_acad":"InstitutodeTrabajoSocial","anio":2014,"egre":25,"ingre":51,"postu":70,"ejec":0}]


function getIDs (cod){
  var objects = [];
  for (var i=0; i<json.length; i++){
    if (json[i].cod_uni_acad == cod)
      objects.push(json[i]);
  }
  return objects;
}

function consolidate (json){
  var new_json = [];
  for (var i=0; i<json.length; i++){
    new_object = {};
    new_object.id = json[i][0].cod_uni_acad;
    new_object.name = json[i][0].uni_acad;
    new_object.ingresantes = [];
    new_object.egresados = [];
    new_object.postulantes = [];
    new_object.presupuesto = [];
    new_array = [];
    for (var j=0; j<=4; j++){
      new_object.ingresantes.push([json[i][j].anio, json[i][j].ingre]);
      new_object.egresados.push([json[i][j].anio, json[i][j].egre]);
      new_object.postulantes.push([json[i][j].anio, json[i][j].postu]);
      new_object.presupuesto.push([json[i][j].anio, json[i][j].ejec]);
    }
	//console.log(new_object);
    new_json.push(new_object);
  }
  return new_json;
}

new_json = [];
for (var i=1; i<=14; i++){
  new_json.push(getIDs(i));
}

final_json = consolidate(new_json);

console.log(JSON.stringify(final_json));



//filtros
Math.min.apply(null, data.map(function(faq){
	return Math.min.apply(null, faq.ingresantes.map(function(array) {
		return array[1];}));)
	});
